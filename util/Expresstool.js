const inquirer = require('inquirer');
/**
 * 获取express的选择选项：
 * @type {{version: string}}
 */
const expressStr = {
  version:''
}


async function expresstool(){
  await inquirer.prompt([ {
    type: 'confirm',
    name: 'express',
    message: 'Are you sure to use the express generator to enhance the template?',
    default: true
  }]).then( async (answers) => {
    if(answers.express){
        expressStr.version = true;
    }else {
      expressStr.version = false;
    }
  });
};


module.exports = {
  expressStr,
  expresstool
}
