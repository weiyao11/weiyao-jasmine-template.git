const inquirer = require('inquirer');

const projectStr = {
  projectTemplate:''
}

/**
 * 项目工具模板
 * @returns {Promise<void>}
 * @constructor
 */
async function ProjectTool() {

  await  inquirer.prompt([
    {
      type: 'list',
      message: 'Please choose your template',
      name: 'template',
      choices:[
        "vue",
        "express",
        "koa2",
      ],
      filter:function (value) {
        return value.toLowerCase();
      }
    }
  ]).then(async answers =>{
      projectStr.projectTemplate = answers.template;
  });
};


module.exports = {
  projectStr,
  ProjectTool
}