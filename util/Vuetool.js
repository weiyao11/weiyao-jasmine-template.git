const inquirer = require('inquirer');
/**
 * 获取vue的选择选项：
 * @type {{version: string}}
 */
const  vuetoolStr = {
  version:''
}

/**
 * vue版本选择
 * @returns {Promise<void>}
 */
async function vuetool(){
  await  inquirer.prompt([
    {
      type: 'list',
      message: 'Do you choose vue2.x or vue3.x?',
      name: 'Vue',
      choices:[
        "vue2.x",
        "vue3.x"
      ],
      filter:function (value) {
        return value.toLowerCase();
      }
    }
  ]).then(async answers =>{
    switch (answers.Vue) {
      case 'vue2.x':
        vuetoolStr.version = 'vue2.x';
        break;
      case 'vue3.x':
        vuetoolStr.version = 'vue3.x';
        break;

    }
  });
};


module.exports = {
  vuetool,
  vuetoolStr
}