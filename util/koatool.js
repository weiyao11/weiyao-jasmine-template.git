const inquirer = require('inquirer');
/**
 * 获取express的选择选项：
 * @type {{version: string}}
 */
const koaStr = {
  version:''
}


async function koatool(){
  await inquirer.prompt([ {
    type: 'confirm',
    name: 'koa2',
    message: 'Are you sure to use the koa generator to enhance the template?',
    default: true
  }]).then( async (answers) => {
    if(answers.koa){
      koaStr.version = true;
    }else {
      koaStr.version = false;
    }
  });
};


module.exports = {
  koaStr,
  koatool
}
