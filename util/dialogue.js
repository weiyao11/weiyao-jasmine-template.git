  
 const inquirer = require('inquirer');

  const toolStr = {
    ToolDownload:''
  }

 /**
  * 下载工具的选择函数
  * @returns {Promise<void>}
  */
 async function tool() {
   await  inquirer.prompt([
     {
       type: 'list',
       message: 'Please select the version tool you want, on your computer.',
       name: 'tool',
       choices:[
         "npm",
         "cnpm",
         "yarn"
       ],
       filter:function (value) {
         return value.toLowerCase();
       }
     }
   ]).then(async answers =>{

     switch (answers.tool) {
       case 'npm':
         toolStr.ToolDownload = 'npm';
         break;
       case 'cnpm':
         toolStr.ToolDownload = 'cnpm';
         break;
       case 'yarn':
         toolStr.ToolDownload = 'yarn';
         break;
     }
   });
};

 module.exports = {
   tool,
   toolStr
 }