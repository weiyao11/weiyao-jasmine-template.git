### weiyao-jasmine-templale

#### 介绍

wjt是一个开源的,免费的前端代码生成器,个人开发使用

#### 特点:

- 支持多个前端框架
    - express
    - vue
    - koa2
    - ...
 - 支持后期开发,可扩展性强
 
 
 #### 功能
 
 - wjt init  `<name>`
    
    通过wjt init 命令输入命令行,它与vue等脚手架创建项目几乎一曲同工
    
 
 
 ### 项目:
 
  - Gitee : https://gitee.com/weiyao11/weiyao-jasmine-template.git
  
  - github:
    

