const {promisify} = require("util");

module.exports.clone = async  function (repo,desc) {
  //从github中下载库的插件
  const download = promisify(require('download-git-repo'));
  //加载进度条(定制进度条)
  const ora = require('ora');

  const process = ora(`download...${repo},Please wait...`);
  process.start(); //转圈
  await download(repo,desc);
  process.succeed();
}