const {promisify} = require('util');

const figlet = promisify(require('figlet'));

const clear = require('clear');

//粉笔模块
const chalk = require('chalk');

//控制台选择模块
const inquirer = require('inquirer');
const loggerSuccess = content => console.log(chalk.green(content));
const loggererr = content => console.log(chalk.red(content));

/*
引入下载github的js文件
*/
const {clone} = require('./downloadGitHub');
const {tool,toolStr} = require('../util/dialogue');
const {vuetoolStr,vuetool} = require('../util/Vuetool');
const {expresstool,expressStr} = require('../util/Expresstool');
const {ProjectTool,projectStr} = require('../util/Project');
const {koaStr,koatool} = require('../util/koatool');
/* promisiy化spawn
 对接输出流*/
const spawn = async (...args) => {
  const { spawn } =
    require('child_process');
  return new Promise(resolve => {
    const proc = spawn(...args);
    proc.stdout.pipe(process.stdout)
    proc.stderr.pipe(process.stderr)
    proc.on('close', () => {
      resolve()
    })
  })
};


module.exports = async name=>{
  //  打印欢迎页面
  clear();
  const welcomeData = await  figlet('WJT');
  loggerSuccess(welcomeData);
  loggerSuccess(`cleate a project ${name}`)
        /*
        *1.选择模板
        */
        await ProjectTool();
      /*
      选择下载包的工具 yarn npm cnpm
      */
        await tool();
      switch (projectStr.projectTemplate) {
        case "vue":
          await vuetool();
          //下载vue模板文件
          if(vuetoolStr.version ==='vue2.x'){
            await clone('github:weiyao-mumu/wjt-vue-template',name);
          }else{
            loggererr('暂时没有该模板');
            return ;
          }

          break;
        case "koa2":
            await koatool();
            if(koaStr.version){
              await clone('github:weiyao-mumu/wjt-koa2-template',name);
            }else {
              loggererr('I\'m really sad leaving you, hope I can meet you again next time');
              return ;
            }
          //
        case "express":
          await expresstool();
          if(expressStr.version){
            await clone('github:weiyao-mumu/wjt-express-template',name);
          }else {
            loggererr('I\'m really sad leaving you, hope I can meet you again next time');
            return ;
          }
            break;
      };






   /*
   自动安装依赖
   */
  loggerSuccess('Installation dependencies...,please wait...');

  /*
  *选择对应的依赖安装工具下载
  */
  switch (toolStr.ToolDownload) {
    case "npm":
      try {
        await spawn(process.platform === 'win32' ? 'npm.cmd' : 'npm', ['install'], { cwd: `./${name}` });
      }catch (e) {
        throw new Error('npm自动下载包错误,请自行下载');
      }
    break;
    case "cnpm":
      try {
        await spawn(process.platform === 'win32' ? 'cnpm.cmd' : 'cnpm', ['install'], { cwd: `./${name}` });
      }catch (e) {
        throw new Error('cnpm自动下载包错误,请自行下载');
      }
      break;
    case "yarn":
      try {
        await spawn(process.platform === 'win32' ? 'yarn.cmd' : 'yarn', ['install'], { cwd: `./${name}` });
      }catch (e) {
        throw new Error('yarn自动下载包错误,请自行下载');
      }
    break;
    default:
      loggererr("电脑没有npm cnpm yarn...?")
  };


  loggerSuccess(`
 The installation is complete
  To get Start:
===========================
    cd ${name}
    npm run serve
===========================
  `);
}