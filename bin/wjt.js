#!/usr/bin/env node
let program = require('commander');
//查看版本号
program.version(require('../package.json').version);
//定义命令

program
  .command('init <name>')
  .description('Please select a template,If you are not sure about the specific characters, you can enter the "--help" command')
  .action(require('../lib/init.js'));


program.parse(process.argv);